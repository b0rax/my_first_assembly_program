/* A minimal working example of a C program calling 
 * simple functions written in x86
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

extern uint8_t lower(uint8_t i);
extern uint8_t upper(uint8_t i);
extern bool validate(uint8_t i);

// Driver to test my two assembly functions 
int main(int argc, char ** argv)
{
	// Remind user to supply a value for i
	if (argc < 2)
	{
		printf("Usage: %s <integer>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	int i = atoi(argv[1]);

	// Easier to debug by running each function separately before using results
	uint8_t l = lower(i);

	printf("got past lower()\n");
	fflush(stdout);

	uint8_t u = upper(i);
	bool v = validate(i);

	printf("got here\n");

	// Display results
	printf("lower(%d) = %d\n", i, (int) l);
	printf("upper(%d) = %d\n", i, (int) u);
	printf("validate(%d) = %d\n", i, (int) v);

	return 0;
}
