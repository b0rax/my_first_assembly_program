.section .text
.type lower, @function
.type upper, @function
.type validate, @function

.globl lower
# This version doesn't work, but I think it should.
# I thought ebx gets the first function parameter.
lower:
	xor %eax, %eax
	# Move 8-bit function parameter to ebx
	movl 4(%esp), %ebx
	shr $0x1, %ebx
	mov %ebx, %eax
	# trying to make code not seg fault later
	#mov %ebp, %esp
	#pop %ebp
	ret

.globl upper
upper:
	# Just return 0xFF for now
	mov $0xff, %eax

	# trying to make code not seg fault later
	#mov %ebp, %esp
	#pop %ebp
	ret

.globl validate
validate:
	# Just return false for now
	xor %eax, %eax

	# trying to make code not seg fault later
	#mov %ebp, %esp
	#pop %ebp
	ret
