# my_first_assembly_program

This is the first thing I've written in x86. Syntax is AT&T.  

I'm having trouble with the **ABI**. I thought a single function parameter would be
passed into *ebx* and the return value would go in *eax*.

By default, on an x86_64 machine, gcc was using the System V AMD64 ABI. Doing an *apt-get install gcc-multilib* made it possible to compile as 32-bit and get cdecl as the default calling convention.

Compilation:
> as -32 -o functions.o functions.s  
> gcc -m32 -o program main.c functions.o  
